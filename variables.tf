variable "capabilities" {
  type = object({
    add  = set(string)
    drop = set(string)
  })
  default = {
    add  = []
    drop = []
  }
  description = "the capabilities of the docker container"
}

variable "image" {
  type        = string
  description = "the image of the container"
}

variable "name" {
  type        = string
  description = "the name of the container"
}

variable "tag" {
  type        = string
  description = "the tag of the container"
}

variable "command" {
  type        = list(string)
  description = "the command to run in the container"
}

variable "entrypoint" {
  type        = list(string)
  description = "Used to override the entryproint of the container"
}

variable "user" {
  type        = string
  default     = null
  description = "the user running the process in the container"
}

variable "upload" {
  type = map(object({
    file           = string
    executable     = bool
    content_base64 = string
  }))
  default     = {}
  description = "used to upload files to the container on deploy"
}

variable "ports" {
  type = map(object({
    internal = number
    external = number
  }))
  default     = {}
  description = "the ports the container should bind to"
}

variable "volumes" {
  type = map(object({
    container_path = string
    host_path      = string
  }))
  default     = {}
  description = "used to specify host mounts if needed"
}

variable "docker_volumes" {
  type = map(object({
    container_path = string
  }))
  default     = {}
  description = "used to specify docker volumes"
}

variable "env" {
  type        = list(string)
  default     = []
  description = "environment variables passed to the container"
}

variable "networks_advanced" {
  type        = map(object({}))
  default     = {}
  description = "networks to attach to the container"
}

variable "labels" {
  type = map(object({
    value = string
  }))
  default     = {}
  description = "additional container labels"
}

variable "start" {
  type        = bool
  default     = true
  description = "only create, or start the container on deployment"
}

variable "dns" {
  type        = list(string)
  default     = []
  description = "additional dns servers passed to the container"
}

variable "container_lifecycle" {
  type = map(object({
    prevent_destroy = bool
  }))
  default = {
    prevent_destroy = {
      prevent_destroy = false
    }
  }
}
