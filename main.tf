terraform {
  required_version = ">= 1.0.1"
  required_providers {
    docker = {
      source  = "kreuzwerker/docker"
      version = "2.15.0"
    }
  }
}

resource "docker_container" "container" {
  image      = "${var.image}:${var.tag}"
  name       = var.name
  entrypoint = var.entrypoint
  command    = var.command
  user       = var.user
  env        = var.env
  dns        = var.dns

  capabilities {
    add  = var.capabilities.add
    drop = var.capabilities.drop
  }

  dynamic "volumes" {
    for_each = var.docker_volumes
    content {
      volume_name    = volumes.key
      container_path = volumes.value.container_path
    }
  }

  dynamic "volumes" {
    for_each = var.volumes
    content {
      container_path = volumes.value.container_path
      host_path      = volumes.value.host_path
    }
  }

  dynamic "ports" {
    for_each = var.ports
    content {
      internal = ports.value.internal
      external = ports.value.external
    }
  }

  dynamic "upload" {
    for_each = var.upload
    content {
      file           = upload.value.file
      executable     = upload.value.executable
      content_base64 = base64encode(upload.value.content_base64)
    }
  }

  dynamic "networks_advanced" {
    for_each = var.networks_advanced
    content {
      name = networks_advanced.key
    }
  }

  dynamic "labels" {
    for_each = var.labels
    content {
      label = labels.key
      value = labels.value.value
    }
  }

  dynamic "lifecycle" {
    for_each = var.container_lifecycle
    content {
      prevent_destroy = lifecycle.prevent_destroy
    }
  }
}
